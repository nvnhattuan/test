﻿#include <iostream>

using namespace std;

// B1: Khai báo cấu trúc cây.
struct Node
{
	int _IData;
	Node* PtrLeft;
	Node* PtrRight;
};
typedef Node* Tree;

// B2: Khởi tạo cây.
void _init(Tree*& Root) // Node*& Root
{
	Root = nullptr;
}

// B3: Tạo node.
Node* _createNode(int _IData)
{
	Node* PtrNode = new Node;
	if (PtrNode == nullptr)
	{
		return nullptr;
	}
	PtrNode->_IData = _IData;
	PtrNode->PtrLeft = PtrNode->PtrRight = nullptr;
	return PtrNode;
}

// B4: Nhập dữ liệu cho cây(tạo cây).
void _insertNodeTree(Tree& t, int x)
{
	if (t == nullptr)
	{
		Node* PtrNode = _createNode(x);
		PtrNode->PtrLeft = PtrNode->PtrRight = nullptr;
		t = PtrNode;
	}
	else
	{
		if (t->_IData > x)
		{
			_insertNodeTree(t->PtrLeft, x); // insert left.
		}
		if (t->_IData < x)
		{
			_insertNodeTree(t->PtrRight, x); // insert right.
		}
	}
}

// B5: Xử lý
void NLR(Tree t)
{
	if (t != nullptr)
	{
		cout << t->_IData << " ";
		NLR(t->PtrLeft); // Left.
		NLR(t->PtrRight); // Right.
	}
}

// B6: Giải phóng cây.
void _freeTree(Tree& t)
{
	if (t != nullptr)
	{
		_freeTree(t->PtrLeft);
		_freeTree(t->PtrRight);

		delete t;
		t = nullptr;
	}
}

int _treeHight_Recursion(Node* _Root)
{
	if (_Root == nullptr)
	{
		return 0;
	}

	int _ILeft = _treeHight_Recursion(_Root->PtrLeft);
	int _IRight = _treeHight_Recursion(_Root->PtrRight);
	return (_ILeft > _IRight ? _ILeft + 1 : _IRight + 1);
}

void _treeHight_TailRecursion(Node* _Root, int& _IMaxHight, int _IHight)
{
	if (_Root != nullptr)
	{
		if (_IHight > _IMaxHight)
		{
			_IMaxHight = _IHight;
		}
		++_IHight;
		_treeHight_TailRecursion(_Root->PtrLeft, _IMaxHight, _IHight);
		_treeHight_TailRecursion(_Root->PtrRight, _IMaxHight, _IHight);
	}
}

int main()
{
	cout << "Ahihi" << endl;

	system("pause");
	return 0;
}
