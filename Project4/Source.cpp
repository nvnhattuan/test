#include <iostream>
#include <cstdio>
#include <cctype>

using namespace std;

void _writeToFile(FILE* _FileOut)
{
	// Ghi vao file voi ham fprintf_s.
	for (size_t i = 0; i < 5; i++)
	{
		fprintf_s(_FileOut, "Nguyen Nhat Tuan %d\n", i + 1);
	}
}

void _readFromFile(FILE* _FileIn)
{
	char _Str[500];
	int c = 1;
	while (fgets(_Str, 500, _FileIn) != NULL)
	{
		cout << "Lan: " << c++ << endl;
		cout << _Str << endl;
	}
}

int sizeOfFile(FILE* _File)
{
	fseek(_File, 0, SEEK_END);
	int _Size = ftell(_File);
	fseek(_File, 0, SEEK_SET);

	return _Size;
}

int main()
{
	// Doc file.
	FILE* FileIn;
	errno_t err_in = fopen_s(&FileIn, "output.txt", "w+");
	if (err_in)
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}
	_writeToFile(FileIn);
	rewind(FileIn);
	_readFromFile(FileIn);
	cout << (char)toupper(65) << endl;
	//cout << sizeOfFile(FileIn) << endl;

	fclose(FileIn);

	system("pause");
	return 0;
}
