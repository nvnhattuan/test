//#include <iostream>
//
//using namespace std;
//
//int _sum(int _IN);
//int _sumKhuDeQuy(int _IN);
//int _sumDeQuyDuoi(int _IN, int _IX = 1);
//int _factorial(int _IN);
//int _fibonacy(int _IN);
//double _sum1(double _IX, int _IN);
//int _tinhXN(int _IN);
//bool _isEven(int _IN);
//bool _isOdd(int _IN);
//void _arrayInput(int* _IArr, int _IN);
//void _matrixInput(int** _IMatrix, int _IRows, int _ICols, int i = 0);
//
//int main()
//{
//	//cout << "Tong tu 1 -> 5: " << _sum(5) << endl;
//	//cout << "KhuDeQuy: Tong tu 1 -> 5: " << _sumKhuDeQuy(5) << endl;
//	//cout << "5!: " << _factorial(10) << endl;
//	//cout << "So hang thu n Fibonacy: " << _fibonacy(5) << endl;
//
//	//int _IN = 5;
//	////cout << "isEven: " << _isEven(_IN) << endl;
//	//cout << "isOdd: " << _isOdd(_IN) << endl;
//
//	int a[100];
//	_arrayInput(a, 5);
//	
//	system("pause");
//	return 0;
//}
//
//int _sum(int _IN)
//{
//	if (_IN == 1)
//		return 1;
//	return (_sum(_IN - 1) + _IN);
//}
//
//int _sumKhuDeQuy(int _IN)
//{
//	int _ISum = 0;
//	for (size_t i = 0; i <= _IN; i++)
//	{
//		_ISum += i;
//	}
//	return _ISum;
//}
//
//int _sumDeQuyDuoi(int _IN, int _IX)
//{
//	if (_IN == 1)
//		return _IX;
//	return _sumDeQuyDuoi(_IN - 1, _IX + _IN);
//}
//
//int _factorial(int _IN)
//{
//	if (_IN == 1)
//		return 1;
//	return _IN * _factorial(_IN - 1);
//}
//
//int _fibonacy(int _IN)
//{
//	if (_IN == 0 || _IN == 1)
//		return 1;
//	return _fibonacy(_IN - 1) + _fibonacy(_IN - 2);
//}
//
//double _sum1(double _IX, int _IN)
//{
//	if (!_IN)
//		return 0.0;
//	if (_IN == 1)
//		return _IX;
//	return (1 + (_IX / _IN)) * _sum1(_IX, _IN - 1) - (_IX / _IN) * _sum1(_IX, _IN - 2);
//}
//
//int _tinhXN(int _IN)
//{
//	if (_IN == 0)
//		return 1;
//	int _ISum = 0;
//	for (size_t i = 0; i <= _IN; i++)
//	{
//		_ISum += i * i * _tinhXN(_IN - 1);
//	}
//	return _ISum;
//}
//
//bool _isEven(int _IN)
//{
//	if (_IN == 0)
//		return true;
//	return _isOdd(_IN - 1);
//}
//
//bool _isOdd(int _IN)
//{
//	if (_IN == 1)
//		return true;
//	return _isEven(_IN - 1);
//}
//
//void _arrayInput(int* _IArr, int _IN)
//{
//	if (_IN == 0)
//		return;
//	_arrayInput(_IArr, _IN - 1);
//	cout << "Nhap a[" << _IN - 1 << "] = ";
//	cin >> _IArr[_IN - 1];
//}
//
//void _matrixInput(int** _IMatrix, int _IRows, int _ICols, int i = 0)
//{
//	if (i == _IRows * _ICols)
//		return;
//	cout << "Nhap a[" << i / _ICols << ", " << i % _ICols << "] = ";
//	cin >> _IMatrix[i / _ICols][i % _ICols];
//	_matrixInput(_IMatrix, _IRows, _ICols, i + 1);
//}

#include <iostream>

using namespace std;

class HinhChuNhat {
private:
	int chieuDai;
	int chieuRong;
public:
	HinhChuNhat(int chieuDai, int chieuRong) {
		this->chieuDai = chieuDai;
		this->chieuRong = chieuRong;
	}

	HinhChuNhat() : chieuDai(0) { }

	friend int HienThiChieuDai(HinhChuNhat); //friend function
};

int HienThiChieuDai(HinhChuNhat hcn) {
	hcn.chieuDai += 10;
	hcn.chieuRong += 10;
	return hcn.chieuDai;
}

int main() {
	HinhChuNhat hcn = HinhChuNhat(10, 20);
	cout << "Chieu dai cua hinh chu nhat la: " << HienThiChieuDai(hcn) << endl;

	return 0;
}
