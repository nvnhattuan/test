#include <iostream>
#include <fstream>

using namespace std;

int main()
{
	ifstream _FileIn("input.txt", ios_base::in);
	//_FileIn.open("input.txt", ios_base::in);

	if (_FileIn.fail())
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}

	while (!_FileIn.eof())
	{
		int _IN;
		if (_FileIn >> _IN)
			cout << _IN << endl;
		else
		{
			break;
		}
	}

	_FileIn.close();

	system("pause");
	return 0;
}
