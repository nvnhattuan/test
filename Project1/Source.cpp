//#define _CRT_SECURE_NO_WARNINGS

#include <iostream>
#include <cstdio>

using namespace std;

int main()
{
	// Doc file.
	FILE* _FileIn;
	errno_t err = fopen_s(&_FileIn, "input.txt", "r");

	//cout << "err: " << err << endl;

	if (!err)
	{
		int _IA, _IB;
		//char _Str[255];
		//fscanf_s(_FileIn, "%[^\n]", _Str, __crt_countof(_Str));
		//cout << "_Str = " << _Str << ", Length = " << strlen(_Str) << endl;

		//fscanf_s(_FileIn, "%d%d", &_IA, &_IB);
		fscanf_s(_FileIn, "%d", &_IA);
		fscanf_s(_FileIn, "%d", &_IB);
		cout << "a = " << _IA << ", b = " << _IB << endl;

		fclose(_FileIn);
	}

	// Ghi file.
	/*FILE* FileOut;
	errno_t err = fopen_s(&FileOut, "output.txt", "w");
	if (err)
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}*/

	//char _C;
	//cout << "Nhap 1 ky tu: ";
	//cin >> _C;

	// Ghi 1 ky tu vao file.
	//cout << fputc('a', FileOut) << endl;

	// Ghi 1 chuoi vao file
	//cout << fputs("Nguyen Nhat Tuan", FileOut);

	// Ghi vao file voi ham fprintf_s.
	/*for (size_t i = 0; i < 5; i++)
	{
		cout << fprintf_s(FileOut, "Nguyen Nhat Tuan %d\n", i) << endl;
	}

	fclose(FileOut);*/

	system("pause");
	return 0;
}
