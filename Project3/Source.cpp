#include <iostream>
#include <cstdio>

using namespace std;

void _writeToFile(FILE* _FileOut)
{
	// Ghi vao file voi ham fprintf_s.
	for (size_t i = 0; i < 5; i++)
	{
		fprintf_s(_FileOut, "Nguyen Nhat Tuan %d\n", i + 1);
	}
}

void _readFromFile(FILE* _FileIn)
{
	char _Str[500];
	int c = 1;
	while (fgets(_Str, 500, _FileIn) != NULL)
	{
		cout << "Lan: " << c++ << endl;
		cout << _Str << endl;
	}
}

int main()
{
	// Ghi file.
	FILE* FileOut;
	errno_t err_out = fopen_s(&FileOut, "output.txt", "w");
	if (err_out)
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}

	_writeToFile(FileOut);
	fclose(FileOut);

	// Doc file.
	FILE* FileIn;
	errno_t err_in = fopen_s(&FileIn, "output.txt", "r");
	if (err_in)
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}
	_readFromFile(FileIn);

	fclose(FileIn);

	system("pause");
	return 0;
}
