﻿#include <iostream>
#include <Windows.h>
#include <stack>
#include <list>
#include <queue>

using namespace std;

// 1. Khai báo cấu trúc dữ liệu cho danh sách liên kết - số nguyên.
struct _SNode
{
	int _IData;
	_SNode* _PNext;
};

struct _SList
{
	_SNode* _PHead;
	_SNode* _PTail;
};

// 2. Khởi tạo DSLK.
void _init(_SList& _Lst);

// 3. Tạo 1 node cho DSLK.
_SNode* _createNode(int _IData);

// 4. Add head/tail cho DSLK.
void _addHead(_SList& _Lst, _SNode* _PN);
void _addTail(_SList& _Lst, _SNode* _PN);

// 5. Thiết kế hàm input/output.
void _input(_SList& _Lst);
void _output(_SList _Lst);

// 6. Xử lý các yêu cầu cho DSLK.
int _linkedSumOfElement(_SList _Lst);
bool _descending(int _IVar1, int _IVar2);
bool _ascending(int _IVar1, int _IVar2);
void _linkedSort(_SList& _Lst, bool (*Compar)(int, int) = _descending);
// Thêm
// Thêm nút x vào sau nút q.
void _linkedListAddLast(_SList& _Lst, _SNode* x, _SNode* q);
// Thêm x vào sau tất cả số chẵn.
void _linkedListAddLastEven(_SList& _Lst, int _IX);
// Thêm x vào trước nút q.
void _linkedListAddFront(_SList& _Lst, _SNode* x, _SNode* q);
// Thêm vào trước các số chẵn.
void _linkedListAddFrontEvens(_SList& _Lst, int _IDataAdd);
// Xóa
// Xóa 1 node nằm sau nút q.
void _linkedListEraseLast(_SList& _Lst, _SNode* q);
// Xóa hết tất cả số chẵn.
void _linkedListEraseEvenAll(_SList& _Lst);
void _linkedListEraseTop(_SList& _Lst); // Xóa đầu.
void _linkedListDeleteLast(_SList& _Lst); // Xóa cuối.

// 7. Giải phóng các phần tử trong DSLK.
void _freeLinked(_SList& _Lst);

int main()
{
	//_SList _Lst;
	//_input(_Lst);
	//_output(_Lst);

	//cout << "\nTong gia tri: " << _linkedSumOfElement(_Lst) << endl;

	////_linkedSort(_Lst, _ascending);
	////cout << "Danh sach sau khi sap xep giam dan: ";

	//_SNode* q, * x;
	//q = _createNode(2);
	//x = _createNode(69);

	////_linkedListAddFront(_Lst, x, q);
	////_linkedListAddFrontEvens(_Lst, 69);

	////_linkedListEraseLast(_Lst, q);
	//_linkedListEraseEvenAll(_Lst);

	//_output(_Lst);
	//cout << endl;

	//_freeLinked(_Lst);

	stack<int> _ISA;

	_ISA.pop();

	queue<int> _ISQ;

	system("pause");
	return 0;
}

void _init(_SList& _Lst)
{
	_Lst._PHead = _Lst._PTail = NULL;
}

_SNode* _createNode(int _IData)
{
	_SNode* _PNode = new _SNode;
	if (_PNode == nullptr)
	{
		return NULL;
	}
	_PNode->_IData = _IData;
	_PNode->_PNext = nullptr;

	return _PNode;
}

void _addHead(_SList& _Lst, _SNode* _PN)
{
	if (_Lst._PHead == nullptr)
	{
		_Lst._PHead = _Lst._PTail = _PN;
	}
	else
	{
		_PN->_PNext = _Lst._PHead;
		_Lst._PHead = _PN;
	}
}

void _addTail(_SList& _Lst, _SNode* _PN)
{
	if (_Lst._PHead == nullptr)
	{
		_Lst._PHead = _Lst._PTail = _PN;
	}
	else
	{
		_Lst._PTail->_PNext = _PN; // Cho nút cuối cùng trỏ vào nút vừa tạo.
		_Lst._PTail = _PN; // Cập nhật lại pTail.
	}
}

void _input(_SList& _Lst)
{
	_init(_Lst); // Khởi tạo danh sách.
	// Nhập vào số lượng phần tử cần thêm vào.
	int _ILength;
	cout << "Nhap vao so luong phan tu: ";
	cin >> _ILength;

	for (size_t i = 0; i < _ILength; i++)
	{
		int _IData;
		cout << "Nhap vao gia tri cho nut thu " << i + 1 << ": ";
		cin >> _IData;

		_SNode* _PNode = _createNode(_IData);
		//_addHead(_Lst, _PNode);
		_addTail(_Lst, _PNode);
	}
}

void _output(_SList _Lst)
{
	_SNode* _PNode;
	for (_PNode = _Lst._PHead; _PNode != nullptr; _PNode = _PNode->_PNext)
	{
		cout << _PNode->_IData << " ";
	}
}

int _linkedSumOfElement(_SList _Lst)
{
	if (_Lst._PHead != NULL)
	{
		int _ISum = 0;
		for (_SNode* _PNode = _Lst._PHead; _PNode != NULL; _PNode = _PNode->_PNext)
		{
			_ISum += _PNode->_IData;
		}
		return _ISum;
	}
	return NULL;
}

bool _descending(int _IVar1, int _IVar2)
{
	return (_IVar1 > _IVar2);
}

bool _ascending(int _IVar1, int _IVar2)
{
	return (_IVar1 < _IVar2);
}

void _linkedSort(_SList& _Lst, bool(*Compar)(int, int))
{
	for (_SNode* _PNode = _Lst._PHead; _PNode != _Lst._PTail; _PNode = _PNode->_PNext)
	{
		for (_SNode* _QNode = _PNode->_PNext; _QNode != NULL; _QNode = _QNode->_PNext)
			if (Compar(_PNode->_IData, _QNode->_IData))
				swap(_PNode->_IData, _QNode->_IData);
	}
}

void _linkedListAddLast(_SList& _Lst, _SNode* x, _SNode* q)
{
	for (_SNode* p = _Lst._PHead; p != NULL; p = p->_PNext)
	{
		if (p->_IData == q->_IData)
		{
			_SNode* g = p->_PNext;
			x->_PNext = g;
			p->_PNext = x;
			return;
		}
	}
}

void _linkedListAddLastEven(_SList& _Lst, int _IX)
{
	for (_SNode* p = _Lst._PHead; p != NULL; p = p->_PNext)
	{
		if (!(p->_IData % 2))
		{
			_SNode* x = _createNode(_IX);
			_SNode* g = p->_PNext;
			x->_PNext = g;
			p->_PNext = x;
			p = p->_PNext;
		}
	}
}

void _linkedListAddFront(_SList& _Lst, _SNode* x, _SNode* q)
{
	if (q->_IData == _Lst._PHead->_IData)
	{
		_addHead(_Lst, x);
		return;
	}
	_SNode* _Temp = nullptr; // Nut nam truoc.
	for (_SNode* i = _Lst._PHead; i != nullptr; i = i->_PNext)
	{
		if (i->_IData == q->_IData)
		{
			x->_PNext = i;
			_Temp->_PNext = x;
			return;
		}
		_Temp = i;
	}
}

void _linkedListAddFrontEvens(_SList& _Lst, int _IDataAdd)
{
	_SNode* _Temp = _Lst._PHead;
	for (_SNode* i = _Lst._PHead->_PNext; i != nullptr; i = i->_PNext)
	{
		if (!(i->_IData % 2))
		{
			_SNode* x = _createNode(_IDataAdd);
			x->_PNext = i;
			_Temp->_PNext = x;
		}
		_Temp = i;
	}
}

void _linkedListEraseLast(_SList& _Lst, _SNode* q)
{
	for (_SNode* i = _Lst._PHead; i != _Lst._PTail; i = i->_PNext)
	{
		if (i->_IData == q->_IData)
		{
			_SNode* _Temp = i->_PNext;
			i->_PNext = _Temp->_PNext;
			delete _Temp;
			return;
		}
	}
}

void _linkedListEraseEvenAll(_SList& _Lst)
{
	_SNode* _NodeLast = _Lst._PHead; // Nút nằm trước.
	for (_SNode* i = _Lst._PHead->_PNext; i != _Lst._PTail; i = i->_PNext)
	{
		if (!(i->_IData % 2)) {
			_SNode* _NodeNext = i->_PNext;
			_NodeLast->_PNext = _NodeNext;
			delete i;
			i = _NodeLast;
		}
		_NodeLast = i;
	}
	if (!(_Lst._PHead->_IData % 2)) {
		_linkedListEraseTop(_Lst);
	}
	if (!(_Lst._PTail->_IData % 2)) {
		_linkedListDeleteLast(_Lst);
	}
}

void _linkedListEraseTop(_SList& _Lst)
{
	if (_Lst._PHead != NULL)
	{
		_SNode* p = _Lst._PHead;
		_Lst._PHead = _Lst._PHead->_PNext;
		delete p;
	}
}

void _linkedListDeleteLast(_SList& _Lst)
{
	_SNode* g = nullptr;
	for (_SNode* p = _Lst._PHead; p != NULL; p = p->_PNext)
	{
		if (p == _Lst._PTail)
		{
			g->_PNext = NULL;
			_Lst._PTail = g;
			delete p;
			return;
		}
		g = p;
	}
}

void _freeLinked(_SList& _Lst)
{
	_SNode* _PNode; // Tạo nút ảo.
	while (_Lst._PHead != nullptr)
	{
		_PNode = _Lst._PHead; // Cho nút ảo trỏ vào đầu.
		_Lst._PHead = _Lst._PHead->_PNext; // Cho pHead trỏ vào phần tử kế tiếp.
		delete _PNode;
	}
}
