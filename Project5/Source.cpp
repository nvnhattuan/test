#include <iostream>
#include <cstdio>
#include <fstream>

using namespace std;

struct _Date {
	int _Day;
	int _Month;
	int _Year;
};

_Date _day = { 15, 2, 1997 };

int main()
{
	// Ghi file.
	FILE* FileOut;
	errno_t err = fopen_s(&FileOut, "output.dat", "w");
	if (err)
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}

	if (fwrite(&_day, sizeof(_Date), 1, FileOut) != 1)
		cout << "Ghi du lieu khong thanh cong." << endl;

	fclose(FileOut);

	FILE* FileIn;
	errno_t errIn = fopen_s(&FileIn, "output.dat", "r");
	if (errIn)
	{
		cout << "File khong ton tai!" << endl;
		system("pause");
		return 0;
	}

	_Date _dayIn;
	if (fread_s(&_dayIn, sizeof(_dayIn), sizeof(_Date), 1, FileIn) != 1)
		cout << "Ghi du lieu khong thanh cong." << endl;
	else
	{
		cout << "Day: " << _dayIn._Day << endl;
		cout << "Month: " << _dayIn._Month << endl;
		cout << "Year: " << _dayIn._Year << endl;
	}

	fclose(FileIn);

	system("pause");
	return 0;
}
