﻿/*
Write a program that sets variables for an online order and displays the contents of the order with the shipping information:



Price of item 1 = $15.95

Price of item 2 = $24.95

Price of item 3 = $6.95

Price of item 4 = $12.95

Price of item 5 = $3.95



The shipping information:

Your full name

Your address with city, state, and zipcode

Your telephone number



All of the data: item prices, name, address, etc should be assigned to the correctly typed variables and then outputted. For the items, use separate cout statements; however, for the shipping information show this with a single cout statement to display all of this information.
*/

//#include <iostream>
//
//using namespace std;
//
//int main(int argc, const char* argv[]) {
//    double price_Item1 = 15.95;
//    double price_Item2 = 24.95;
//    double price_Item3 = 6.95;
//    double price_Item4 = 12.95;
//    double price_Item5 = 3.95;
//    cout << "Price of item 1 = $" << price_Item1 << endl;
//    cout << "Price of item 2 = $" << price_Item2 << endl;
//    cout << "Price of item 3 = $" << price_Item3 << endl;
//    cout << "Price of item 4 = $" << price_Item4 << endl;
//    cout << "Price of item 5 = $" << price_Item5 << endl;
//    cout << "The Shipping Information: " << endl;
//    cout << "Your Full name: " << "Gia Phan" << endl;
//    cout << "Your address with city, state, and zipcode: " << "4315 Box Elder Path, Gainesville, GA, 30504 " << endl;
//    cout << "Your telephone number: " << "678-316-7574" << endl;
//    return 0;
//}

/*
Assuming there are no deposits other than the original investment, the balance in a savings account after one year may be calculated as  Amount = Principal ∗(1+ Rate T)^T Principal is the balance in the savings account, Rate is the interest rate, and T is the number of times the interest is compounded during a year ( T is 4 if the interest is compounded quarterly). Write a program that asks for the principal, the interest rate, and the number of times the interest is compounded. It should display a report similar to Interest Rate: 4.25% Times Compounded: 12 Principal: $ 1000.00 Interest: $ 43.34 Amount in Savings: $ 1043.34
*/
#include <iostream>
#include <cmath>
#include <iomanip> // setprecision

using namespace std;

int main(int argc, const char* argv[]) {

    //double Amount, Principal, interestRate, interest;
    //int timeCompounded;
    double DAmount, DPrincipal, DInterestRate, DInterest;
    int ITimeCompounded;

    cout << "Enter principal amount: ";
    cin >> DPrincipal;
    cout << "Enter interest rate: ";
    cin >> DInterestRate;
    cout << "Enter number of times interest is compounded: ";
    cin >> ITimeCompounded;

    DAmount = DPrincipal * pow((1 + (DInterestRate / ITimeCompounded)), ITimeCompounded);
    DInterest = DAmount - DPrincipal;
    DInterestRate = DInterestRate * 100;

    cout << "Interest Rate:          " << DInterestRate << "%" << endl;
    cout << "Times Compounded        " << ITimeCompounded << endl;
    cout << "Principal              $" << DPrincipal << endl;
    cout << "Interest               $" << setprecision(4) << DInterest << endl;
    cout << "Amount in saving       $" << setprecision(6) << DAmount << endl;
    //setw(15);

    system("pause");
    return 0;
}